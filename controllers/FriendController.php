<?php

namespace app\controllers;

use Yii;
use app\models\Friend;
use app\models\FriendSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use letsjump\easyAjax\EasyAjax;
use yii\widgets\ActiveForm;
use Faker;

/**
 * FriendController implements the CRUD actions for Friend model.
 */
class FriendController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionResetTable($n = 30)
    {
        Yii::$app->db->createCommand(/** @lang SQLite */ <<<SQL
            delete from user;
            delete from sqlite_sequence where name='user';
         
SQL
)->queryAll();
        $faker = Faker\Factory::create('it_IT');
        for ($i = 0; $i <= $n; $i++) {
            $friend          = new Friend();
            $friend->name    = $faker->name;
            $friend->city    = $faker->city;
            $friend->address = $faker->address;
            $friend->save();
        }
        $this->redirect(['index']);
    }
    
    /**
     * Lists all Friend models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new FriendSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Displays a single Friend model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            return [
                EasyAjax::modalBasic(
                    $this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ])
                )
            ];
        }
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    /**
     * Creates a new Friend model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Friend();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    /**
     * Updates an existing Friend model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    /**
     * Create or update a Friend in a bootstrap modal
     *
     * @param null $id
     *
     * @return array
     */
    public function actionModal($id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model                      = (int)$id > 0 ? $this->findModel($id) : new Friend();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return [
                    EasyAjax::modalClose(),
                    EasyAjax::reloadPjax(['#friend-yea-pjax']),
                    EasyAjax::notifySuccess(
                        $id === null
                            ? Yii::t('app', 'Friend created')
                            : Yii::t('app', 'Friend updated')
                    )
                ];
            }
            
            return [EasyAjax::formValidation(['#friend-form' => ActiveForm::validate($model)])];
        }
        
        return [
            EasyAjax::modal(
                $this->renderAjax('_modal', ['model' => $model]),
                $model->isNewRecord
                    ? Yii::t('app', 'Add a new friend')
                    : Yii::t('app', 'Update friend'),
                'friend-form'
            ),
        ];
    }
    
    /**
     * Deletes an existing Friend model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            
            // EasyAjax: remember to adjust the reloadPjax #ID attribute to reflect the ID attribute of your gridview Pjax container
            return [
                EasyAjax::reloadPjax(['#friend-yea-pjax']),
                EasyAjax::notifyInfo(Yii::t('yii', 'Friend deleted'))
            ];
        }
        
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Friend model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Friend the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Friend::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}

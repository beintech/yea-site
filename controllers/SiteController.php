<?php
/**
 * @package   yii2-easy-ajax
 * @author    Gianpaolo Scrigna <letsjump@gmail.com>
 * @link      https://github.com/letsjump/yii2-easy-ajax
 * @copyright Copyright &copy; Gianpaolo Scrigna, beintech.it, 2019
 * @version   $version
 */

namespace app\controllers;

use app\models\ContactForm;
use letsjump\easyAjax\EasyAjax;
use letsjump\easyAjax\helpers\Modal;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $contactForm = new ContactForm();
        
        return $this->render('index', [
            'contactForm' => $contactForm,
        ]);
    }
    
    /**
     * Displays a simple landing page.
     *
     * @return string
     */
    public function actionLanding()
    {
        return $this->render('landing');
    }
    
    public function actionLinkTutorial()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::modalBasic(
                $this->renderAjax('_link-tutorial'),
                'Add to your link'
            )
        ];
    }
    
    public function actionConfirm()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::Confirm('This will fire a growl. Ok?', Url::to(['site/notify']))
        ];
    }
    
    public function actionNotify($type = 'info')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        switch ($type) {
            case 'success':
                return [
                    EasyAjax::notifySuccess('This is a Success Notify!')
                ];
                break;
            case 'info':
            default:
                return [
                    EasyAjax::notifyInfo('This is an Info Nofify!')
                ];
                break;
            case 'warning':
                return [
                    EasyAjax::notifyWarning('This is a Warning Nofify!')
                ];
                break;
            case 'danger':
                return [
                    EasyAjax::notifyDanger('This is a Danger Nofify!', "DANGEROUS ACTION!<br>", ['timer' => 15000])
                ];
                break;
        }
        
    }
    
    public function actionBasicModal($size = Modal::SIZE_DEFAULT)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::modalBasic('This is the modal content', 'Modal title', $size),
        ];
    }
    
    public function actionBasicModalNoFooter($size = Modal::SIZE_DEFAULT)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::modalBasic('A small modal without a footer', 'Modal title', $size, false),
        ];
    }
    
    public function actionAjaxRedirect()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::redirectAjax(Url::to(['site/notify']))
        ];
    }
    
    public function actionJavascriptRedirect()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::redirectJavascript(Url::to(['site/landing']))
        ];
    }
    
    public function actionContentReplace()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::contentReplace(['#time' => date('d/m/Y H:i:s')])
        ];
    }
    
    public function actionPjaxReload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::reloadPjax(['#test0'])
        ];
    }
    
    public function actionTab($id)
    {
        $date = new \DateTimeImmutable('now');
        
        $content = [
            'rome' => $date->setTimezone(new \DateTimeZone('Europe/Rome'))->format('l jS \of F h:i:s A'),
            'london' => $date->setTimezone(new \DateTimeZone('Europe/London'))->format('l jS \of F h:i:s A'),
            'new-york' => $date->setTimezone(new \DateTimeZone('America/New_York'))->format('l jS \of F h:i:s A'),
            'calcutta' => $date->setTimezone(new \DateTimeZone('Asia/Calcutta'))->format('l jS \of F h:i:s A'),
        ];
        
        if ( ! array_key_exists($id, $content)) {
            throw new BadRequestHttpException('Your request is invalid');
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::tab($id, '<p>' . $content[$id] . '</p>')
        ];
    }
    
    public function actionValidateForm()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $contactForm                = new ContactForm();
        if ($contactForm->load(Yii::$app->request->post())) {
            return [
                EasyAjax::formValidation(['#contact-form' => ActiveForm::validate($contactForm)])
            ];
        }
    }

//    /**
//     * Login action.
//     *
//     * @return Response|string
//     */
//    public function actionLogin()
//    {
//        if ( ! Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        }
//
//        $model->password = '';
//
//        return $this->render('login', [
//            'model' => $model,
//        ]);
//    }

//    /**
//     * Logout action.
//     *
//     * @return Response
//     */
//    public function actionLogout()
//    {
//        Yii::$app->user->logout();
//
//        return $this->goHome();
//    }
    
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            
            return $this->refresh();
        }
        
        return $this->render('contact', [
            'model' => $model,
        ]);
    }
    
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}

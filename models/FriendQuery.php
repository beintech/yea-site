<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Friend]].
 *
 * @see Friend
 */
class FriendQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Friend[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Friend|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

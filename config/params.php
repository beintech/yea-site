<?php

return [
    'adminEmail' => 'admin@example.com',
    'easyAjax'   => [
        //'viewPath' => '@vendor/letsjump/yii2-easy-ajax/views',
        'modal'    => [
            'viewFile' => '_modal_default',
            'modal_id' => 'yea-modal',
        ],
        'notify'   => [
            'viewFile'    => '_notify_default',
            'iconSuccess' => 'glyphicon glyphicon-ok-circle',
            'iconInfo'    => 'glyphicon glyphicon-info-sign',
            'iconWarning' => 'glyphicon glyphicon-warning-sign',
            'iconDanger'  => 'glyphicon glyphicon-exclamation-sign',
            'clientSettings'    => [
//                'element'         => 'body',
//                'position'        => null,
//                'type'            => 'info',
//                'allow_dismiss'   => true,
//                'newest_on_top'   => false,
//                'showProgressbar' => false,
//                'placement'       => [
//                    'from'  => 'top',
//                    'align' => 'right'
//                ],
//                'offset'          => 20,
//                'spacing'         => 10,
//                'z_index'         => 1031,
//                'delay'           => 5000,
//                'timer'           => 1000,
//                'url_target'      => '_blank',
//                'mouse_over'      => null,
//                'animate'         => [
//                    'enter' => 'animated fadeInDown',
//                    'exit'  => 'animated fadeOutUp',
//                ],
//                'onShow'          => null,
//                'onShown'         => null,
//                'onClose'         => null,
//                'onClosed'        => null,
//                'iconType'        => 'class'
            ]
        ],
    ],
];

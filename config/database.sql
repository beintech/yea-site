/*
 *
 *  * @package   yii2-easy-ajax
 *  * @author    Gianpaolo Scrigna <letsjump@gmail.com>
 *  * @link https://github.com/letsjump/yii2-easy-ajax
 *  * @copyright Copyright &copy; Gianpaolo Scrigna, beintech.it, 2017-2020
 *  * @version   1.0.0
 *
 */

create table user
(
    id int not null /*autoincrement needs PK*/,
    name string(32),
    city string(32),
    address string(128)
);

create unique index user_id_uindex
    on user (id);


<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\FriendSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Friends');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="friend-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Friend'), ['modal'], ['class' => 'btn btn-success', 'data-yea'=>1]) ?>
    </p>

    <?php Pjax::begin(['id'=> 'friend-yea-pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name:ntext',
            'city:ntext',
            'address:ntext',

            [
                'class' => 'letsjump\easyAjax\helpers\ActionColumn',
                'modal' => true
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

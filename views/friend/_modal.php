<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Friend */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'id' => 'friend-form'
]); ?>

<?= $form->field($model, 'name')->textInput() ?>

<?= $form->field($model, 'city')->textInput() ?>

<?= $form->field($model, 'address')->textInput() ?>

<?php ActiveForm::end(); ?>
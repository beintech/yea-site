<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Friend */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Friends'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="friend-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['modal', 'id' => $model->id], ['class' => 'btn btn-primary', 'data-yea'=>1]) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data-yea'=>1,
            'data' => [
                'data-pjax' => 0,
                'yea-confirm'  => Yii::t('app', 'Are you sure you want to delete this item?'),
                'yea-method' => 'post',
                'dismiss' => 'modal'
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name:ntext',
            'city:ntext',
            'address:ntext',
        ],
    ]) ?>

</div>

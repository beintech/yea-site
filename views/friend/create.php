<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Friend */

$this->title = Yii::t('app', 'Create Friend');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Friends'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="friend-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

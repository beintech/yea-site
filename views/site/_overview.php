<?php
/**
 *
 *  * @package   yii2-easy-ajax
 *  * @author    Gianpaolo Scrigna <letsjump@gmail.com>
 *  * @link https://github.com/letsjump/yii2-easy-ajax
 *  * @copyright Copyright &copy; Gianpaolo Scrigna, beintech.it, 2017-2020
 *  * @version   1.0.0
 *
 */

?>
<div class="bs-docs-section">
    <h1 id="js-overview" class="page-header">[A] Overview</h1>

    <h2 id="js-individual-compiled">[A] Individual or compiled</h2>
    <p>Plugins can be included individually (using Bootstrap's individual <code>*.js</code> files), or all at once
        (using <code>bootstrap.js</code> or the minified <code>bootstrap.min.js</code>).</p>

    <div class="bs-callout bs-callout-danger" id="callout-overview-not-both">
        <h4>Using the compiled JavaScript</h4>
        <p>Both <code>bootstrap.js</code> and <code>bootstrap.min.js</code> contain all plugins in a single file.
            Include only one.</p>
    </div>

    <div class="bs-callout bs-callout-danger" id="callout-overview-dependencies">
        <h4>Plugin dependencies</h4>
        <p>Some plugins and CSS components depend on other plugins. If you include plugins individually, make sure to
            check for these dependencies in the docs. Also note that all plugins depend on jQuery (this means jQuery
            must be included <strong>before</strong> the plugin files). <a
                    href="https://github.com/twbs/bootstrap/blob/v3-dev/bower.json">Consult our <code>bower.json</code></a>
            to see which versions of jQuery are supported.</p>
    </div>

    <h2 id="js-data-attrs">Data
        attributes</h2>
    <p>You can use all Bootstrap plugins purely through the markup API without writing a single line of JavaScript. This
        is Bootstrap's first-class API and should be your first consideration when using a plugin.</p>

    <p>That said, in some situations it may be desirable to turn this functionality off. Therefore, we also provide the
        ability to disable the data attribute API by unbinding all events on the document namespaced with
        <code>data-api</code>. This looks like this:</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight">
        <pre><code class="language-js" data-lang="js"><span class="nx">$</span><span class="p">(</span><span class="nb">document</span><span
                        class="p">).</span><span class="nx">off</span><span class="p">(</span><span class="s1">'.data-api'</span><span
                        class="p">)</span></code></pre>
    </figure>

    <p>Alternatively, to target a specific plugin, just include the plugin's name as a namespace along with the data-api
        namespace like this:</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight">
        <pre><code class="language-js" data-lang="js"><span class="nx">$</span><span class="p">(</span><span class="nb">document</span><span
                        class="p">).</span><span class="nx">off</span><span class="p">(</span><span class="s1">'.alert.data-api'</span><span
                        class="p">)</span></code></pre>
    </figure>

    <div class="bs-callout bs-callout-danger" id="callout-overview-single-data">
        <h4>Only one plugin per element via data attributes</h4>
        <p>Don't use data attributes from multiple plugins on the same element. For example, a button cannot both have a
            tooltip and toggle a modal. To accomplish this, use a wrapping element.</p>
    </div>

    <h2 id="js-programmatic-api"><a class="anchorjs-link " href="#js-programmatic-api"
                                    aria-label="Anchor link for: js programmatic api" data-anchorjs-icon=""
                                    style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 1em; line-height: inherit; font-family: anchorjs-icons; font-size-adjust: none; font-kerning: auto; font-optical-sizing: auto; font-language-override: normal; font-feature-settings: normal; font-variation-settings: normal; position: absolute; margin-left: -1em; padding-right: 0.5em;"></a>Programmatic
        API</h2>
    <p>We also believe you should be able to use all Bootstrap plugins purely through the JavaScript API. All public
        APIs are single, chainable methods, and return the collection acted upon.</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight">
        <pre><code class="language-js" data-lang="js"><span class="nx">$</span><span class="p">(</span><span class="s1">'.btn.danger'</span><span
                        class="p">).</span><span class="nx">button</span><span class="p">(</span><span class="s1">'toggle'</span><span
                        class="p">).</span><span class="nx">addClass</span><span class="p">(</span><span class="s1">'fat'</span><span
                        class="p">)</span></code></pre>
    </figure>

    <p>All methods should accept an optional options object, a string which targets a particular method, or nothing
        (which initiates a plugin with default behavior):</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight"><pre><code class="language-js" data-lang="js"><span class="nx">$</span><span
                        class="p">(</span><span class="s1">'#myModal'</span><span class="p">).</span><span class="nx">modal</span><span
                        class="p">()</span>                      <span class="c1">// initialized with defaults</span>
<span class="nx">$</span><span class="p">(</span><span class="s1">'#myModal'</span><span class="p">).</span><span
                        class="nx">modal</span><span class="p">({</span> <span class="na">keyboard</span><span
                        class="p">:</span> <span class="kc">false</span> <span class="p">})</span>   <span class="c1">// initialized with no keyboard</span>
<span class="nx">$</span><span class="p">(</span><span class="s1">'#myModal'</span><span class="p">).</span><span
                        class="nx">modal</span><span class="p">(</span><span class="s1">'show'</span><span
                        class="p">)</span>                <span
                        class="c1">// initializes and invokes show immediately</span></code></pre>
    </figure>

    <p>Each plugin also exposes its raw constructor on a <code>Constructor</code> property: <code>$.fn.popover.Constructor</code>.
        If you'd like to get a particular plugin instance, retrieve it directly from an element: <code>$('[rel="popover"]').data('popover')</code>.
    </p>

    <h4 id="default-settings"><a class="anchorjs-link " href="#default-settings"
                                 aria-label="Anchor link for: default settings" data-anchorjs-icon=""
                                 style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 1em; line-height: inherit; font-family: anchorjs-icons; font-size-adjust: none; font-kerning: auto; font-optical-sizing: auto; font-language-override: normal; font-feature-settings: normal; font-variation-settings: normal; position: absolute; margin-left: -1em; padding-right: 0.5em;"></a>Default
        settings</h4>
    <p>You can change the default settings for a plugin by modifying the plugin's <code>Constructor.DEFAULTS</code>
        object:</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight">
        <pre><code class="language-js" data-lang="js"><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span
                        class="p">.</span><span class="nx">modal</span><span class="p">.</span><span class="nx">Constructor</span><span
                        class="p">.</span><span class="nx">DEFAULTS</span><span class="p">.</span><span class="nx">keyboard</span> <span
                        class="o">=</span> <span class="kc">false</span> <span class="c1">// changes default for the modal plugin's `keyboard` option to false</span></code></pre>
    </figure>

    <h2 id="js-noconflict"><a class="anchorjs-link " href="#js-noconflict" aria-label="Anchor link for: js noconflict"
                              data-anchorjs-icon=""
                              style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 1em; line-height: inherit; font-family: anchorjs-icons; font-size-adjust: none; font-kerning: auto; font-optical-sizing: auto; font-language-override: normal; font-feature-settings: normal; font-variation-settings: normal; position: absolute; margin-left: -1em; padding-right: 0.5em;"></a>No
        conflict</h2>
    <p>Sometimes it is necessary to use Bootstrap plugins with other UI frameworks. In these circumstances, namespace
        collisions can occasionally occur. If this happens, you may call <code>.noConflict</code> on the plugin you wish
        to revert the value of.</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight"><pre><code class="language-js" data-lang="js"><span class="kd">var</span> <span
                        class="nx">bootstrapButton</span> <span class="o">=</span> <span class="nx">$</span><span
                        class="p">.</span><span class="nx">fn</span><span class="p">.</span><span
                        class="nx">button</span><span class="p">.</span><span class="nx">noConflict</span><span
                        class="p">()</span> <span class="c1">// return $.fn.button to previously assigned value</span>
<span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span class="p">.</span><span class="nx">bootstrapBtn</span> <span
                        class="o">=</span> <span class="nx">bootstrapButton</span>            <span class="c1">// give $().bootstrapBtn the Bootstrap functionality</span></code></pre>
    </figure>

    <h2 id="js-events"><a class="anchorjs-link " href="#js-events" aria-label="Anchor link for: js events"
                          data-anchorjs-icon=""
                          style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 1em; line-height: inherit; font-family: anchorjs-icons; font-size-adjust: none; font-kerning: auto; font-optical-sizing: auto; font-language-override: normal; font-feature-settings: normal; font-variation-settings: normal; position: absolute; margin-left: -1em; padding-right: 0.5em;"></a>Events
    </h2>
    <p>Bootstrap provides custom events for most plugins' unique actions. Generally, these come in an infinitive and
        past participle form - where the infinitive (ex. <code>show</code>) is triggered at the start of an event, and
        its past participle form (ex. <code>shown</code>) is triggered on the completion of an action.</p>
    <p>As of 3.0.0, all Bootstrap events are namespaced.</p>
    <p>All infinitive events provide <code>preventDefault</code> functionality. This provides the ability to stop the
        execution of an action before it starts.</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight"><pre><code class="language-js" data-lang="js"><span class="nx">$</span><span
                        class="p">(</span><span class="s1">'#myModal'</span><span class="p">).</span><span
                        class="nx">on</span><span class="p">(</span><span class="s1">'show.bs.modal'</span><span
                        class="p">,</span> <span class="kd">function</span> <span class="p">(</span><span
                        class="nx">e</span><span class="p">)</span> <span class="p">{</span>
  <span class="k">if</span> <span class="p">(</span><span class="o">!</span><span class="nx">data</span><span class="p">)</span> <span
                        class="k">return</span> <span class="nx">e</span><span class="p">.</span><span class="nx">preventDefault</span><span
                        class="p">()</span> <span class="c1">// stops modal from being shown</span>
<span class="p">})</span></code></pre>
    </figure>

    <h2 id="js-sanitizer"><a class="anchorjs-link " href="#js-sanitizer" aria-label="Anchor link for: js sanitizer"
                             data-anchorjs-icon=""
                             style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 1em; line-height: inherit; font-family: anchorjs-icons; font-size-adjust: none; font-kerning: auto; font-optical-sizing: auto; font-language-override: normal; font-feature-settings: normal; font-variation-settings: normal; position: absolute; margin-left: -1em; padding-right: 0.5em;"></a>Sanitizer
    </h2>

    <p>Tooltips and Popovers use our built-in sanitizer to sanitize options which accept HTML.</p>
    <p>The default <code>whiteList</code> value is the following:</p>

    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight"><pre><code class="language-js" data-lang="js"><span class="kd">var</span> <span
                        class="nx">ARIA_ATTRIBUTE_PATTERN</span> <span class="o">=</span> <span
                        class="sr">/^aria-</span><span class="se">[\w</span><span class="sr">-</span><span
                        class="se">]</span><span class="sr">*$/i</span>
<span class="kd">var</span> <span class="nx">DefaultWhitelist</span> <span class="o">=</span> <span class="p">{</span>
  <span class="c1">// Global attributes allowed on any supplied element below.</span>
  <span class="s1">'*'</span><span class="p">:</span> <span class="p">[</span><span class="s1">'class'</span><span
                        class="p">,</span> <span class="s1">'dir'</span><span class="p">,</span> <span
                        class="s1">'id'</span><span class="p">,</span> <span class="s1">'lang'</span><span
                        class="p">,</span> <span class="s1">'role'</span><span class="p">,</span> <span class="nx">ARIA_ATTRIBUTE_PATTERN</span><span
                        class="p">],</span>
  <span class="na">a</span><span class="p">:</span> <span class="p">[</span><span class="s1">'target'</span><span
                        class="p">,</span> <span class="s1">'href'</span><span class="p">,</span> <span class="s1">'title'</span><span
                        class="p">,</span> <span class="s1">'rel'</span><span class="p">],</span>
  <span class="na">area</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">b</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">br</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">col</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">code</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">div</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">em</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">hr</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">h1</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">h2</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">h3</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">h4</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">h5</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">h6</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">i</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">img</span><span class="p">:</span> <span class="p">[</span><span class="s1">'src'</span><span
                        class="p">,</span> <span class="s1">'alt'</span><span class="p">,</span> <span class="s1">'title'</span><span
                        class="p">,</span> <span class="s1">'width'</span><span class="p">,</span> <span class="s1">'height'</span><span
                        class="p">],</span>
  <span class="na">li</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">ol</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">p</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">pre</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">s</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">small</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">span</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">sub</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">sup</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">strong</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">u</span><span class="p">:</span> <span class="p">[],</span>
  <span class="na">ul</span><span class="p">:</span> <span class="p">[]</span>
<span class="p">}</span></code></pre>
    </figure>

    <p>If you want to add new values to this default <code>whiteList</code> you can do the following:</p>

    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight"><pre><code class="language-js" data-lang="js"><span class="kd">var</span> <span
                        class="nx">myDefaultWhiteList</span> <span class="o">=</span> <span class="nx">$</span><span
                        class="p">.</span><span class="nx">fn</span><span class="p">.</span><span
                        class="nx">tooltip</span><span class="p">.</span><span class="nx">Constructor</span><span
                        class="p">.</span><span class="nx">DEFAULTS</span><span class="p">.</span><span class="nx">whiteList</span>

<span class="c1">// To allow table elements</span>
<span class="nx">myDefaultWhiteList</span><span class="p">.</span><span class="nx">table</span> <span class="o">=</span> <span
                        class="p">[]</span>

<span class="c1">// To allow td elements and data-option attributes on td elements</span>
<span class="nx">myDefaultWhiteList</span><span class="p">.</span><span class="nx">td</span> <span
                        class="o">=</span> <span class="p">[</span><span class="s1">'data-option'</span><span class="p">]</span>

<span class="c1">// You can push your custom regex to validate your attributes.</span>
<span class="c1">// Be careful about your regular expressions being too lax</span>
<span class="kd">var</span> <span class="nx">myCustomRegex</span> <span class="o">=</span> <span class="sr">/^data-my-app-</span><span
                        class="se">[\w</span><span class="sr">-</span><span class="se">]</span><span
                        class="sr">+/</span>
<span class="nx">myDefaultWhiteList</span><span class="p">[</span><span class="s1">'*'</span><span
                        class="p">].</span><span class="nx">push</span><span class="p">(</span><span class="nx">myCustomRegex</span><span
                        class="p">)</span></code></pre>
    </figure>

    <p>If you want to bypass our sanitizer because you prefer to use a dedicated library, for example <a
                href="https://www.npmjs.com/package/dompurify">DOMPurify</a>, you should do the following:</p>

    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight"><pre><code class="language-js" data-lang="js"><span class="nx">$</span><span
                        class="p">(</span><span class="s1">'#yourTooltip'</span><span class="p">).</span><span
                        class="nx">tooltip</span><span class="p">({</span>
  <span class="na">sanitizeFn</span><span class="p">:</span> <span class="kd">function</span> <span
                        class="p">(</span><span class="nx">content</span><span class="p">)</span> <span
                        class="p">{</span>
    <span class="k">return</span> <span class="nx">DOMPurify</span><span class="p">.</span><span
                        class="nx">sanitize</span><span class="p">(</span><span class="nx">content</span><span
                        class="p">)</span>
  <span class="p">}</span>
<span class="p">})</span></code></pre>
    </figure>

    <div class="bs-callout bs-callout-danger" id="callout-sanitizer-no-createhtmldocument">
        <h4>Browsers without <code>document.implementation.createHTMLDocument</code></h4>
        <p>In case of browsers that don't support <code>document.implementation.createHTMLDocument</code>, like Internet
            Explorer 8, the built-in sanitize function returns the HTML as is.</p>
        <p>If you want to perform sanitization in this case, please specify <code>sanitizeFn</code> and use an external
            library like <a href="https://www.npmjs.com/package/dompurify">DOMPurify</a>.</p>
    </div>

    <h2 id="js-version-nums"><a class="anchorjs-link " href="#js-version-nums"
                                aria-label="Anchor link for: js version nums" data-anchorjs-icon=""
                                style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 1em; line-height: inherit; font-family: anchorjs-icons; font-size-adjust: none; font-kerning: auto; font-optical-sizing: auto; font-language-override: normal; font-feature-settings: normal; font-variation-settings: normal; position: absolute; margin-left: -1em; padding-right: 0.5em;"></a>Version
        numbers</h2>
    <p>The version of each of Bootstrap's jQuery plugins can be accessed via the <code>VERSION</code> property of the
        plugin's constructor. For example, for the tooltip plugin:</p>
    <div class="bs-clipboard">
        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
    </div>
    <figure class="highlight">
        <pre><code class="language-js" data-lang="js"><span class="nx">$</span><span class="p">.</span><span class="nx">fn</span><span
                        class="p">.</span><span class="nx">tooltip</span><span class="p">.</span><span class="nx">Constructor</span><span
                        class="p">.</span><span class="nx">VERSION</span> <span
                        class="c1">// =&gt; "3.4.1"</span></code></pre>
    </figure>

    <h2 id="js-disabled"><a class="anchorjs-link " href="#js-disabled" aria-label="Anchor link for: js disabled"
                            data-anchorjs-icon=""
                            style="font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 1em; line-height: inherit; font-family: anchorjs-icons; font-size-adjust: none; font-kerning: auto; font-optical-sizing: auto; font-language-override: normal; font-feature-settings: normal; font-variation-settings: normal; position: absolute; margin-left: -1em; padding-right: 0.5em;"></a>No
        special fallbacks when JavaScript is disabled</h2>
    <p>Bootstrap's plugins don't fall back particularly gracefully when JavaScript is disabled. If you care about the
        user experience in this case, use <a
                href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/noscript"><code>&lt;noscript&gt;</code></a>
        to explain the situation (and how to re-enable JavaScript) to your users, and/or add your own custom fallbacks.
    </p>

    <div class="bs-callout bs-callout-warning" id="callout-third-party-libs">
        <h4>Third-party libraries</h4>
        <p><strong>Bootstrap does not officially support third-party JavaScript libraries</strong> like Prototype or
            jQuery UI. Despite <code>.noConflict</code> and namespaced events, there may be compatibility problems that
            you need to fix on your own.</p>
    </div>
</div>



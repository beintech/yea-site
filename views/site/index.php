<?php
/**
 * @package   yii2-easy-ajax
 * @author    Gianpaolo Scrigna <letsjump@gmail.com>
 * @link      https://github.com/letsjump/yii2-easy-ajax
 * @copyright Copyright &copy; Gianpaolo Scrigna, beintech.it, 2019
 * @version   $version
 */

use dominus77\highlight\Plugin;
use yii\helpers\Url as Url;


/**
 * @var $this        yii\web\View
 * @var $contactForm \app\models\ContactForm
 */

$this->title = 'Easy Ajax - Yii2';

Plugin::$options = [
    'theme'       => 'atom-one-light',
    'lineNumbers' => false,    // Show line numbers
    'singleLine'  => false,     // Show number if one line
];
/** $this \yii\web\View */
Plugin::register($this);
\letsjump\AnchorJS\AnchorJS::widget([
    'add'     => 'h2',
    'options' => ['placement' => 'right'],
]);

?>
<div class="site-index">
    <div class="row">
        <div class="col-md-9" role="main">
            
            <?= '' // $this->render('_overview')      ?>

            <div class="row">
                <div class="col-lg-12">
                    <h2 id="notify">Notify Growls</h2>
                    <div class="panel panel-default">
                        <div class="panel-heading">CONTROLLER</div>
                        <div class="panel-body">
                            <p>Remember to add a property <code>data-yea="1"</code> in a link pointing to your yii2
                                controller
                                action (<a data-yea="1" href="<?= Url::to(['site/link-tutorial']) ?>">example</a>)
                            </p>
                            <h4>Success Notify (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/notify', 'type' => 'success'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <pre><code class="php">    public function actionNotify()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            \letsjump\easyAjax\EasyAjax::notifySuccess('This is a Success Notify!')
        ];
    }</code></pre>

                            <h4>Info Notify (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/notify', 'type' => 'info'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <pre><code class="php">    public function actionNotify()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            \letsjump\easyAjax\EasyAjax::notifyInfo('This is an Info Notify!')
        ];
    }</code></pre>

                            <h4>Warning Notify (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/notify', 'type' => 'warning'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <pre><code class="php">    public function actionNotify()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            \letsjump\easyAjax\EasyAjax::notifyWarning('This is a Warning Notify!')
        ];
    }</code></pre>

                            <h4>Danger Notify (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/notify', 'type' => 'danger'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <p>A danger notify with a personalized timer setting</p>
                            <pre><code class="php">    public function actionNotify()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            \letsjump\easyAjax\EasyAjax::notifyDanger('This is a Danger Notify!', null, ['timer' => 15000])
        ];
    }</code></pre>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h2 id="helpers">Other helpers</h2>
                    <div class="panel panel-default">
                        <div class="panel-heading">CONTROLLER</div>
                        <div class="panel-body">
                            <p>Remember to add a property <code>data-yea="1"</code> in a link pointing to your yii2
                                controller
                                action (<a data-yea="1" href="<?= Url::to(['site/link-tutorial']) ?>">example</a>)
                            </p>
                            <h4>Confirm alert (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/confirm'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <pre><code class="php">    public function actionConfirm()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::confirm('This will fire a growl. Ok?', Url::to(['site/notify']))
        ];
    }</code></pre>

                            <h4>Ajax Redirect (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/ajax-redirect'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <p>This will redirect to an ajax action. For example, this will fire another growl using
                                site/notify action</p>
                            <pre><code class="php">    public function actionAjaxRedirect()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            EasyAjax::redirectAjax(Url::to(['site/notify']))
        ];
    }</code></pre>

                            <h4>Javascript Redirect (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/javascript-redirect'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <p>This will redirect to a landing page by <code>window.location.href</code></p>
                            <pre><code class="php">    public function actionJavascriptRedirect()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            EasyAjax::redirectJavascript(Url::to(['site/landing']))
        ];
    }</code></pre>

                            <h4>Content replace (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/content-replace'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <p>Last click time is <code id="time">never clicked</code></p>
                            <pre><code class="php">    public function actionContentReplace()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::contentReplace(['#time' => date('d/m/Y H:i:s')])
        ];
    }</code></pre>

                            <h4>Form Validation (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/validate-form'],
                                    [
                                        'data-form-id' => 'contact-form',
                                        'data-yea'     => 1
                                        //'class'       => 'modalform-submit'
                                    ]
                                ) ?>)</h4>
                            <?= $this->render('_contact_form', ['model' => $contactForm]) ?>
                            <pre><code class="php">    public function actionValidateForm()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $contactForm                = new ContactForm();
        
        return [
            EasyAjax::formValidation(['#contact-form' => ActiveForm::validate($contactForm)])
        ];
    }</code></pre>

                            <h4>Pjax Reload (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/pjax-reload'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <?php \yii\widgets\Pjax::begin(['id' => 'test0']) ?>
                            <?= $this->render('/site/_pjax') ?>
                            <?php \yii\widgets\Pjax::end() ?>
                            <pre><code class="php">    public function actionPjaxReload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::reloadPjax(['#test0'])
        ];
    }</code></pre>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h2 id="modals">Modals</h2>
                    <div class="panel panel-default">
                        <div class="panel-heading">CONTROLLER</div>
                        <div class="panel-body">
                            <p>Remember to add a property <code>data-yea="1"</code> in a link pointing to your yii2
                                controller
                                action (<a data-yea="1" href="<?= Url::to(['site/link-tutorial']) ?>">example</a>)
                            </p>
                            <h4>Basic modal (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/basic-modal'],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <pre><code class="php">    public function actionBasicModal()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::modalBasic('This is the modal content', 'Modal title'),
        ];
    }</code></pre>

                            <h4>Basic modal without footer (<?= \yii\helpers\Html::a(
                                    'Try it',
                                    ['site/basic-modal-no-footer', 'size' => \yii\bootstrap\Modal::SIZE_SMALL],
                                    ['data-yea' => 1]
                                ) ?>)</h4>
                            <pre><code class="php">    public function actionBasicModalNoFooter()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        return [
            EasyAjax::modalBasic('This is the modal content', 'Modal title', Modal::SIZE_SMALL, false),
        ];
    }</code></pre>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h2 id="tabs">Tabs</h2>
                    <div class="panel panel-default">
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <?= \yii\bootstrap\Tabs::widget([
                                'items' => [
                                    [
                                        'label' => 'Instructions',
                                        'content' => $this->render('_tab_instructions')
                                    ],
                                    [
                                        'label'       => 'Rome',
                                        'linkOptions' => [
                                            'data-href' => Url::to(['site/tab', 'id' => 'rome',]),
                                            'data-yea'  => 1
                                        ],
                                        'options'     => ['id' => 'rome'],
                                    ],
                                    [
                                        'label'       => 'London',
                                        'linkOptions' => [
                                            'data-href' => Url::to(['site/tab', 'id' => 'london']),
                                            'data-yea'  => 1
                                        ],
                                        'options'     => ['id' => 'london'],
                                    ],
                                    [
                                        'label'       => 'New York',
                                        'linkOptions' => [
                                            'data-href' => Url::to(['site/tab', 'id' => 'new-york',]),
                                            'data-yea'  => 1
                                        ],
                                        'options'     => ['id' => 'new-york'],
                                    ],
                                    [
                                        'label'       => 'Calcutta',
                                        'linkOptions' => [
                                            'data-href' => Url::to(['site/tab', 'id' => 'calcutta',]),
                                            'data-yea'  => 1
                                        ],
                                        'options'     => ['id' => 'calcutta'],
                                    ],
                                ]
                            ]) ?>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3" role="complementary">
            <nav id="nav-left" class="bs-docs-sidebar hidden-print hidden-xs hidden-sm" data-spy="affix"
                 data-offset-top="300">
                <?= \yii\widgets\Menu::widget([
                    'options' => [
                        'class' => 'nav bs-docs-sidenav',
                        //'id' => 'nav-left'
                    ],
                    'items'   => [
                        ['label' => 'Notify', 'url' => '#notify'],
                        ['label' => 'Other helpers', 'url' => '#helpers'],
                        ['label' => 'Modals', 'url' => '#modals'],
                        ['label' => 'Tabs', 'url' => '#tabs'],
                    ]
                ]) ?>
            </nav>
        </div>
    </div>
</div>
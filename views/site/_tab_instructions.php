<p>Click on each tab to get the current local date</p>
<h4>How it works</h4>
<p><strong>View</strong></p>
<pre><code class="php">
&lt;?= \yii\bootstrap\Tabs::widget([
    'items' => [
        [
            'label'       => 'Rome',
            'linkOptions' => [
                'data-href' => Url::to(['site/tab', 'id' => 'rome',]),
                'data-yea'  => 1
            ],
            'options'     => ['id' => 'rome'],
        ],
        // ... other tabs
    ]
]) ?&gt;
    </code> </pre>

<p><strong>Controller</strong></p>
<pre><code class="php">
public function actionTab($id)
{
    $date = new \DateTimeImmutable('now');
    
    $content = [
        'rome' => $date->setTimezone(new \DateTimeZone('Europe/Rome'))->format('l jS \of F h:i:s A'),
        'london' => $date->setTimezone(new \DateTimeZone('Europe/London'))->format('l jS \of F h:i:s A'),
        'new-york' => $date->setTimezone(new \DateTimeZone('America/New_York'))->format('l jS \of F h:i:s A'),
        'calcutta' => $date->setTimezone(new \DateTimeZone('Asia/Calcutta'))->format('l jS \of F h:i:s A'),
    ];
    
    if ( ! array_key_exists($id, $content)) {
        throw new BadRequestHttpException('Your request is invalid');
    }
    
    Yii::$app->response->format = Response::FORMAT_JSON;
    
    return [
        EasyAjax::tab($id, '&lt;p&gt;' . $content[$id] . '&lt;/p&gt;')
    ];
}
    </code> </pre>
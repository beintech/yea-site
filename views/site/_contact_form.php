<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\bootstrap\ActiveForm;
$form = ActiveForm::begin([
    'id' => 'contact-form',
    'enableClientValidation' => false,
    'action' => ['site/validate-form'],
    'enableAjaxValidation' => true,
]);
?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'email') ?>
    </div>
    <div class="col-sm-4">
        <?= $form->field($model, 'subject') ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
<div class="panel panel-default">
    <div class="panel-heading">VIEW</div>
    <div class="panel-body">
        <p>Add a property <code>data-yea="1"</code> in a link pointing to an yii2 controller action</p>
        <pre>
                <code class="html hljs"><?= htmlentities(<<<html
<a class="btn btn-lg btn-success" data-ajax="1" href="<?= \yii\helpers\Url::to(['controller/action-notify']) ?>">notify something</a>
html
                    );
                    ?></code>
                    </pre>
    </div>
</div>
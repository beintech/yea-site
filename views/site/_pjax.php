<?php
/**
 * @package   yii2-easy-ajax
 * @author    Gianpaolo Scrigna <letsjump@gmail.com>
 * @link https://github.com/letsjump/yii2-easy-ajax
 * @copyright Copyright &copy; Gianpaolo Scrigna, beintech.it, 2018
 * @version   $version
 */

/* @var $this yii\web\View */

?>


<p>Last click time is <code id="time"><?= date('d/m/Y H:i:s') ?></code></p>
